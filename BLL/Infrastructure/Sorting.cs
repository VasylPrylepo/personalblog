﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Infrastructure
{
	public enum Sorting
	{
		Date,
		Date_desc,
		Title,
		Title_desc
	}
}
