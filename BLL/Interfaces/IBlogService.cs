﻿using BLL.DTO;
using BLL.Infrastructure;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
	public interface IBlogService
	{
		Task<BlogDto> GetBlogByIdAsync(int id);
		Task CreateBlog(BlogDto blog);
		Task<IEnumerable<BlogDto>> GetBlogsAsync(Sorting? sort);
	}
}
