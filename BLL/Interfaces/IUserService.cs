﻿using BLL.DTO;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IUserService : IDisposable
    {
        Task<bool> Create(ProfileDto userDto,string callBackUrl);
        Task<ClaimsIdentity> Authenticate(ProfileDto userDto);
        Task<ProfileDto> GetProfileByNameAsync(string name);
        Task<ProfileDto> GetProfileByIdAsync(string id);
        Task<IEnumerable<ProfileDto>> GetClients();
        Dictionary<string, string> GetRoles();
        Task<ClaimsIdentity> UpdateProfile(ProfileDto profile,string callBackUrl);
        Task<IdentityResult> Confirm(string id, string code);
        Task<bool> IsConfirmed(string id);
        Task<ClaimsIdentity> ChangeRole(string id);
    }
}
