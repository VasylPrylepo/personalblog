﻿using BLL.DTO;
using BLL.Infrastructure;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
	public interface IArticleService
	{
		Task<IEnumerable<ArticleDto>> GetArticles(Sorting? sort, string tag,int? id);
		Task<ArticleDto> GetArticleByIdAsync(int id);
		Task AddArticleAsync(ArticleDto article);
		Task AddCommentToArticleAsync(CommentDto comment);
		Task AddTagToArticleAsync(TagDto tag);
		Task DeleteTagFromArticleAsync(TagDto tag);
		Task DeleteCommentFromArticleAsync(int commentId);
		Task<IEnumerable<TagDto>> GetTagsAsync();
		Task<IEnumerable<BlogDto>> GetBlogsAsync();
	}
}
