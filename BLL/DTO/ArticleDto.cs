﻿using System;
using System.Collections.Generic;

namespace BLL.DTO
{
	public class ArticleDto
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Text { get; set; }
		public DateTime DateTime { get; set; }
		public string ProfileId { get; set; }
		public string BlogName { get; set; }
		public int BlogId { get; set; }
		public ProfileDto ClientProfile { get; set; }
		public IList<CommentDto> Comments { get; set; }
		public IList<TagDto> Tags { get; set; }
	}
}
