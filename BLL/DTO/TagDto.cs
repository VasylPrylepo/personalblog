﻿

namespace BLL.DTO
{
	public class TagDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int ArticleId { get; set; }
	}
}
