﻿using System;
using System.Collections.Generic;
namespace BLL.DTO
{
	public class BlogDto
	{
		public int Id { get; set; }
		public string ProfileId { get; set; }
		public ProfileDto ClientProfile { get; set; }
		public DateTime DateTime { get; set; } = DateTime.Now;	
		public string Name { get; set; }
		public IEnumerable<ArticleDto> Articles { get; set; }
	}
}
