﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.DTO
{
	public class CommentDto
	{
		public int Id { get; set; }
		public string Text { get; set; }
		public DateTime DateTime { get; set; }
		public string UserName { get; set; }
		public string ProfileId { get; set; }
		public int ArticleId { get; set; }
	}
}
