﻿using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BLL.Services
{
	public class UserService : IUserService
	{
		readonly IUnitOfWork _uow;
		readonly IMapper _mapper;

		public UserService(IUnitOfWork uow,IMapper mapper)
		{
			_uow = uow;
			_mapper = mapper;
		}
		public async Task<ClaimsIdentity> Authenticate(ProfileDto userDto)
		{
			ClaimsIdentity claim = null;

			ApplicationUser user = await _uow.UserManager.FindAsync(userDto.UserName, userDto.Password);

			if (user != null)
				claim = await _uow.UserManager.CreateIdentityAsync(user,
											DefaultAuthenticationTypes.ApplicationCookie);
			return claim;
		}

		public async Task<bool> Create(ProfileDto userDto,string callBackUrl)
		{
			ApplicationUser user = await _uow.UserManager.FindByNameAsync(userDto.UserName);
			if (user == null)
			{
				
				user = new ApplicationUser { Email = userDto.Email, UserName = userDto.UserName };
				var result = await _uow.UserManager.CreateAsync(user, userDto.Password);
				if (result.Errors.Any())
				{
					throw new ArgumentException(result.Errors.ToString());
				}
				await _uow.UserManager.AddToRoleAsync(user.Id, userDto.RoleName);
				_uow.Profiles.Create(new ClientProfile { Id = user.Id, Address = userDto.Address, Name = userDto.Name });
				await _uow.SaveAsync();

				EmailService emailService = new EmailService();
				var id = HttpServerUtility.UrlTokenEncode(Encoding.UTF8.GetBytes(user.Id));
				var code = HttpServerUtility.UrlTokenEncode(
					       Encoding.UTF8.GetBytes(
						  _uow.UserManager.GenerateEmailConfirmationToken(user.Id)));
				var message = new IdentityMessage
				{
					Subject = "Confirm your account",
					Body = $"Confirm your email by this link: <a href={callBackUrl}?userId={id}&code={code}</a>",
					Destination = userDto.Email
				};
				try
				{
					await emailService.SendAsync(message);
				}
				catch
				{
					return false;
				}
				return true;
			}
			else
			{
				throw new InvalidOperationException("User with same UserName or Email is already exists");
			}
		}

		public void Dispose()
		{
			_uow.Dispose();
		}
		public async Task<ProfileDto> GetProfileByNameAsync(string name)
		{
			ApplicationUser user = await _uow.UserManager.FindByNameAsync(name);
			var profile = await _uow.Profiles.GetItemByIdAsync(user.Id);
			return _mapper.Map<ProfileDto>(profile);
		}
		public async Task<ProfileDto> GetProfileByIdAsync(string id)
		{
			var user = await _uow.Profiles.GetItemByIdAsync(id);
			return _mapper.Map<ProfileDto>(user);
		}
		public async Task<IEnumerable<ProfileDto>> GetClients()
		{
			var result = await _uow.Profiles.GetItemsAsync();
			var some = result.ToList();
			return _mapper.Map<IEnumerable<ClientProfile>, IEnumerable<ProfileDto>>(some);
		}
		public Dictionary<string,string> GetRoles()
		{
			Dictionary<string, string> dict = new Dictionary<string, string>();
			foreach(var role in _uow.RoleManager.Roles)
			{
				dict.Add(role.Id, role.Name);
			}
			return dict;
		}
		public async Task<ClaimsIdentity> UpdateProfile(ProfileDto profile,string callBackUrl)
		{
			var users = await _uow.Profiles.GetItemsAsync();
			EmailService email = null;
			ClaimsIdentity claim = null;
			if (users.Any(s=>s.ApplicationUser.Email==profile.Email && s.Id!=profile.Id) || users.Any(s=>s.ApplicationUser.UserName == profile.UserName && s.Id!=profile.Id))
			{
				throw new InvalidOperationException("Email or login is already exists");
			}

			_uow.Profiles.Update(_mapper.Map<ClientProfile>(profile));
			var user = await _uow.UserManager.FindByIdAsync(profile.Id);

			if(user.Email != profile.Email)
			{
				email = new EmailService();
			}

			user.Email = profile.Email;
			user.UserName = profile.UserName;
			if(email != null)
			{
				user.EmailConfirmed = false;
			}
			await _uow.UserManager.UpdateAsync(user);
			await _uow.SaveAsync();

			user = await _uow.UserManager.FindByIdAsync(profile.Id);

			claim = await _uow.UserManager.CreateIdentityAsync(user,
										DefaultAuthenticationTypes.ApplicationCookie);

			if (email != null)
			{
				
				var id = user.Id.Base64ForUrlEncode();
				var code = _uow.UserManager.GenerateEmailConfirmationToken(user.Id).Base64ForUrlEncode();				

				await email.SendAsync(new IdentityMessage
				{
					Subject = "Confirm your account",
					Body = $"Confirm your email by this link: <a href={callBackUrl}?userId={id}&code={code}</a>",
					Destination = profile.Email
				});
			}

			return claim;
		}
		public async Task<IdentityResult> Confirm(string id, string code)
		{
			return await _uow.UserManager.ConfirmEmailAsync(id.Base64ForUrlDecode(), code.Base64ForUrlDecode());
		}
		public async Task<bool> IsConfirmed(string id)
		{
			return await _uow.UserManager.IsEmailConfirmedAsync(id);
		}
		public async Task<ClaimsIdentity> ChangeRole(string id)
		{
			var roles = await _uow.UserManager.GetRolesAsync(id);

			_uow.UserManager.RemoveFromRole(id, roles.First());

			_uow.UserManager.AddToRole(id, roles.First() == "admin" ? "user" : "admin");
			await _uow.SaveAsync();
			var claim = await _uow.UserManager.CreateIdentityAsync(_uow.UserManager.FindById(id),
											DefaultAuthenticationTypes.ApplicationCookie);
			return claim;	
		}
	}
}
