﻿using Microsoft.AspNet.Identity;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace BLL.Services
{ 
	public class EmailService: IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage message)
        {
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(
                 WebConfigurationManager.AppSettings["mailAccount"],
                 WebConfigurationManager.AppSettings["mailPassword"])
            };
            var mess = new MailMessage("gagaru20000410@gmail.com", message.Destination, message.Subject, message.Body);
            await smtp.SendMailAsync(mess);
        }
    }
}
