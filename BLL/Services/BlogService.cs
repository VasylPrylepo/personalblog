﻿using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
	public class BlogService : IBlogService
	{
		private readonly IUnitOfWork _uow;
		private readonly IMapper _mapper;
		public BlogService(IUnitOfWork uow,IMapper mapper)
		{
			_uow = uow;
			_mapper = mapper;
		}
		public async Task CreateBlog(BlogDto blog)
		{
			_uow.Blogs.Create(_mapper.Map<Blog>(blog));
			await _uow.SaveAsync();
		}

		public async Task<BlogDto> GetBlogByIdAsync(int id)
		{
			return _mapper.Map<BlogDto>(await _uow.Blogs.GetItemByIdWithDetailsAsync(id.ToString()));
		}
		public async Task<IEnumerable<BlogDto>> GetBlogsAsync(Sorting? sort)
		{
			IEnumerable<BlogDto> result = _mapper.Map<IEnumerable<Blog>, IEnumerable<BlogDto>>(await _uow.Blogs.GetItemsAsync());
			switch (sort)
			{
				case Sorting.Date:
					result = result.OrderBy(s => s.DateTime);
					break;
				case Sorting.Date_desc:
					result = result.OrderByDescending(s => s.DateTime);
					break;
				case Sorting.Title:
					result = result.OrderBy(s => s.Name);
					break;
				case Sorting.Title_desc:
					result = result.OrderByDescending(s => s.Name);
					break;
			}
			return result;
		}

	}
}