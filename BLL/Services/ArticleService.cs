﻿using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
	public class ArticleService : IArticleService
	{
		private readonly IUnitOfWork _uow;
		private readonly IMapper _mapper;
		public ArticleService(IUnitOfWork unit, IMapper mapper1)
		{
			_uow = unit;
			_mapper = mapper1;
		}
		public async Task AddArticleAsync(ArticleDto article)
		{
			_uow.Articles.Create(_mapper.Map<Article>(article));
			await _uow.SaveAsync();
		}

		public async Task AddCommentToArticleAsync(CommentDto comment)
		{
			_uow.Comments.Create(_mapper.Map<Comment>(comment));
			await _uow.SaveAsync();
		}

		public async Task AddTagToArticleAsync(TagDto tag)
		{
			await _uow.Tags.AddToArticleAsync(tag.Name, tag.ArticleId);
			await _uow.SaveAsync();
		}

		public async Task DeleteTagFromArticleAsync(TagDto tag)
		{
			await _uow.Tags.RemoveFromArticleAsync(tag.Name, tag.ArticleId);
			await _uow.SaveAsync();
		}

		public async Task DeleteCommentFromArticleAsync(int commentId)
		{
			_uow.Comments.Delete(commentId);
			await _uow.SaveAsync();
		}

		public async Task<IEnumerable<ArticleDto>> GetArticles(Sorting? sort, string tag,int? id)
		{
			var some = await _uow.Articles.GetItemsAsync();
			var result = _mapper.Map<IEnumerable<Article>, IEnumerable<ArticleDto>>(some);
			if (!(sort is null))
			{
				switch (sort)
				{
					case Sorting.Date:
						result = result.OrderBy(s => s.DateTime);
						break;
					case Sorting.Date_desc:
						result = result.OrderByDescending(s => s.DateTime);
						break;
					case Sorting.Title:
						result = result.OrderBy(s => s.Title);
						break;
					case Sorting.Title_desc:
						result = result.OrderByDescending(s => s.Title);
						break;
				}
			}
			if ((tag ?? "") != "")
			{
				result = result.Where(s => s.Tags.Any(f => f.Name.ToLower().Replace("#", "") == tag.ToLower()));
			}
			if (!(id is null))
			{
				result = result.Where(s => s.BlogId == id);
			}
			return result;
		}

		public async Task<IEnumerable<TagDto>> GetTagsAsync()
		{
			return _mapper.Map<IEnumerable<Tag>, IEnumerable<TagDto>>(await _uow.Tags.GetItemsAsync());
		}

		public async Task<ArticleDto> GetArticleByIdAsync(int id)
		{
			return _mapper.Map<ArticleDto>(await _uow.Articles.GetItemByIdWithDetailsAsync(id.ToString()));
		}
		public async Task<IEnumerable<BlogDto>> GetBlogsAsync()
		{
			return _mapper.Map<IEnumerable<Blog>, IEnumerable<BlogDto>>(await _uow.Blogs.GetItemsAsync());
		}
	}
}
