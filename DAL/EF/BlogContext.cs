﻿using DAL.Entities;
using DAL.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace DAL.EF
{
	public class BlogContext : IdentityDbContext<ApplicationUser>
	{
		public BlogContext() :base("BlogConnection")
		{
		}
		static BlogContext()
		{
			Database.SetInitializer(new StoreDbInitializer());
		}
		public DbSet<ClientProfile> Clients { get; set; }
		public DbSet<Article> Articles { get; set; }
		public DbSet<Tag> Tags { get; set; }
		public DbSet<Comment> Comments { get; set; }
		public DbSet<Blog> Blogs { get; set; }
	}
	/// <summary>
	/// Custom class to fill our db
	/// </summary>
	public class StoreDbInitializer : DropCreateDatabaseIfModelChanges<BlogContext>
	{
		/// <summary>
		/// Override method that fill db
		/// </summary>
		/// <param name="context">Db</param>
		protected override void Seed(BlogContext context)
		{
			var roles = new ApplicationRoleManager(new RoleStore<ApplicationRole>(context));
			roles.Create(new ApplicationRole() { Name = "user"});
			roles.Create(new ApplicationRole() { Name = "admin" });
			var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context),new DpapiDataProtectionProvider());

			var user = new ApplicationUser { Email = "somemail@gmail.com", UserName = "Alex" };
			manager.Create(user, "1234567");
			manager.AddToRole(user.Id, "admin");

			var client = new ClientProfile
			{
				Name = "Semen Semenovich",
				Address = "Sportyvna street",
				Id = user.Id
			};
			context.Clients.Add(client);
			var blog = new Blog()
			{
				ProfileId = client.Id,
				ClientProfile = client,
				Name = "Programming",
				DateTime = DateTime.Now
			};
			context.Blogs.Add(blog);
			var article = new Article
			{
				Title = "First",
				Text = $"The Boston Celtics have produced some of the biggest names in basketball history: Bill Russell, Red Auerbach, Larry Bird. But among all those icons, Tommy Heinsohn was Mr. Celtic."+
				$"A 6 - foot - 7 forward who smoked cigarettes and sank hook shots,"+
				$"he was a Hall of Fame player and coach who worked with the Celtics from the day he was drafted in 1956 to the day he died,this week,"+
				$"at age 86.That made Heinsohn the only person to play a part in each of the franchise’s 17 championships,"+
				$"a record shared with the Lakers.Despite his starring role in one of sports’ most dominant dynasties,"+
				$"Heinsohn became even better known and loved in Boston for being the Celtics’ color commentator and unofficial mascot for more than four decades.Most New " +
				$"Englanders under 50 remember him not from his time as a championship player and coach, but from having his Boston accent booming into their living rooms every " +
				$"game day.He shouted regularly.He rooted unabashedly.And he excoriated the refs.He did just about everything modern broadcasters are taught not to do, and that was why he was so entertaining."+
                $"“The Celtics were all he ever knew, and it was very, very personal to him,” said Mike Gorman, Heinsohn’s partner behind the mic for nearly 40 years — " +
				$"perhaps the longest tenure for any duo in American sports broadcasting history. “He allowed fans to be over the top, because he was over the top.He was leading the charge.”",
				ClientProfile = client,
				DateTime = DateTime.Now,
				ProfileId = client.Id,
				BlogId = blog.Id,
				Blog = blog
			};
			var tag = new Tag
			{
				Name = "2020"
			};
			var tag1 = new Tag
			{
				Name = "History"
			};
			var listtag = new List<Tag>();
			listtag.Add(tag);
			listtag.Add(tag1);

			context.Tags.AddRange(listtag);

			var comment = new Comment()
			{
				Article = article,
				ClientProfile = client,
				ProfileId = client.Id,
				DateTime = DateTime.Now,
				Text = "Wonderful and amazing"
			};
			var comment1 = new Comment()
			{
				Article = article,
				ClientProfile = client,
				ProfileId = client.Id,
				DateTime = DateTime.Now,
				Text = "I have no idea"
			};
			var listcomments = new List<Comment>();
			listcomments.Add(comment);
			listcomments.Add(comment1);

			context.Comments.AddRange(listcomments);
			article.Comments = listcomments;
			article.Tags = listtag;

			context.Articles.Add(article);
			context.SaveChanges();
		}
	}
}
