﻿using DAL.EF;
using DAL.Entities;
using DAL.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
	public interface IUnitOfWork : IDisposable
	{
		IRepository<Article> Articles { get;}
		IRepository<Comment> Comments { get; }
		IRepository<ClientProfile> Profiles { get; }
		IRepository<Blog> Blogs { get; }
		ITagRepository Tags { get; }
		ApplicationUserManager UserManager { get; }
		ApplicationRoleManager RoleManager { get; }
		Task SaveAsync();
	}
}
