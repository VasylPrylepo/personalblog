﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
	public interface IRepository<T> : IDisposable
	{
		void Create(T item);
		void Delete(int id);
		void Update(T item);
		Task<T> GetItemByIdAsync(string id);
		Task<T> GetItemByIdWithDetailsAsync(string id);
		Task<IEnumerable<T>> GetItemsWithDetailsAsync();
		Task<IEnumerable<T>> GetItemsAsync();
	}
}
