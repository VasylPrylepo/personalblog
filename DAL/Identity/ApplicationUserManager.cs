﻿using DAL.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;

namespace DAL.Identity
{
	public class ApplicationUserManager : UserManager<ApplicationUser>
	{
		public ApplicationUserManager(IUserStore<ApplicationUser> store, IDataProtectionProvider dataProtectionProvider)
				: base(store)
		{
			this.UserValidator = new UserValidator<ApplicationUser>(this)
			{
				AllowOnlyAlphanumericUserNames = true,
				RequireUniqueEmail = true
			};
			if (dataProtectionProvider != null)
			{
				IDataProtector dataProtector = dataProtectionProvider.Create("ASP.NET Identity");
				this.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(dataProtector);
			}
		}
	}
}
