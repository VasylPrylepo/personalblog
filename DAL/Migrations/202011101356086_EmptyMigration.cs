﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmptyMigration : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Articles", new[] { "ClientProfile_Id" });
            DropIndex("dbo.Comments", new[] { "ClientProfile_Id" });
            DropColumn("dbo.Articles", "ProfileId");
            DropColumn("dbo.Comments", "ProfileId");
            RenameColumn(table: "dbo.Articles", name: "ClientProfile_Id", newName: "ProfileId");
            RenameColumn(table: "dbo.Comments", name: "ClientProfile_Id", newName: "ProfileId");
            AlterColumn("dbo.Articles", "ProfileId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Comments", "ProfileId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Articles", "ProfileId");
            CreateIndex("dbo.Comments", "ProfileId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Comments", new[] { "ProfileId" });
            DropIndex("dbo.Articles", new[] { "ProfileId" });
            AlterColumn("dbo.Comments", "ProfileId", c => c.Int(nullable: false));
            AlterColumn("dbo.Articles", "ProfileId", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Comments", name: "ProfileId", newName: "ClientProfile_Id");
            RenameColumn(table: "dbo.Articles", name: "ProfileId", newName: "ClientProfile_Id");
            AddColumn("dbo.Comments", "ProfileId", c => c.Int(nullable: false));
            AddColumn("dbo.Articles", "ProfileId", c => c.Int(nullable: false));
            CreateIndex("dbo.Comments", "ClientProfile_Id");
            CreateIndex("dbo.Articles", "ClientProfile_Id");
        }
    }
}
