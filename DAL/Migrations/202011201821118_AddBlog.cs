﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBlog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Blogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProfileId = c.String(maxLength: 128),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClientProfiles", t => t.ProfileId)
                .Index(t => t.ProfileId);
            
            AddColumn("dbo.Articles", "BlogId", c => c.Int(nullable: false));
            CreateIndex("dbo.Articles", "BlogId");
            AddForeignKey("dbo.Articles", "BlogId", "dbo.Blogs", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Articles", "BlogId", "dbo.Blogs");
            DropForeignKey("dbo.Blogs", "ProfileId", "dbo.ClientProfiles");
            DropIndex("dbo.Blogs", new[] { "ProfileId" });
            DropIndex("dbo.Articles", new[] { "BlogId" });
            DropColumn("dbo.Articles", "BlogId");
            DropTable("dbo.Blogs");
        }
    }
}
