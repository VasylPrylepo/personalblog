﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDateToBlog : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Blogs", "DateTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Blogs", "DateTime");
        }
    }
}
