﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
	public class CommentRepository : IRepository<Comment>
	{
		private readonly BlogContext _context;
		public CommentRepository(BlogContext context)
		{
			_context = context;
		}
		public void Create(Comment item)
		{
			item.ClientProfile = _context.Clients.Find(item.ProfileId);
			_context.Comments.Add(item);
		}

		public void Delete(int id)
		{
			_context.Comments.Remove(_context.Comments.Find(id));
		}

		public async Task<Comment> GetItemByIdAsync(string id)
		{
			var id1 = int.Parse(id);
			return await _context.Comments.FindAsync(id1);
		}
		public async Task<Comment> GetItemByIdWithDetailsAsync(string id)
		{
			var id1 = int.Parse(id);
			return await _context.Comments.Include(s=>s.Article).Include(s=>s.ClientProfile).FirstAsync(s=>s.Id==id1);
		}
		public async Task<IEnumerable<Comment>> GetItemsAsync()
		{
			return await _context.Comments.ToListAsync();
		}
		public async Task<IEnumerable<Comment>> GetItemsWithDetailsAsync()
		{
			return await _context.Comments.Include(s => s.ClientProfile).Include(s => s.Article).ToListAsync();
		}

		public void Update(Comment item)
		{
			var result = _context.Comments.Find(item.Id);
			result.Text = item.Text;
			result.DateTime = item.DateTime;
			result.Article = item.Article;
		}
		public void Dispose()
		{
			_context.Dispose();
		}
	}
}
