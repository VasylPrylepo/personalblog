﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
	public class ProfileRepository : IRepository<ClientProfile>
	{
		private readonly BlogContext _context;
		public ProfileRepository(BlogContext context)
		{
			_context = context;
		}
		public void Create(ClientProfile item)
		{
			_context.Clients.Add(item);
		}

		public void Delete(int id)
		{
			_context.Clients.Remove(_context.Clients.Find(id));
		}

		public async Task<ClientProfile> GetItemByIdAsync(string id)
		{
			return await _context.Clients.FindAsync(id);
		}
		public async Task<ClientProfile> GetItemByIdWithDetailsAsync(string id)
		{
			return await _context.Clients.Include(s=>s.Articles).Include(s=>s.Comments).FirstAsync(s=>s.Id==id);
		}
		public async Task<IEnumerable<ClientProfile>> GetItemsAsync()
		{
			return await _context.Clients.Include(s=>s.ApplicationUser).ToListAsync();
		}
		public async Task<IEnumerable<ClientProfile>> GetItemsWithDetailsAsync()
		{
			return await _context.Clients.Include(s => s.Articles).Include(s => s.Comments).ToListAsync();
		}
		public void Update(ClientProfile item)
		{
			var result = _context.Clients.Find(item.Id);
			result.ApplicationUser.Email = item.ApplicationUser.Email;
			result.Name = item.Name;
			result.Address = item.Address;
			result.ApplicationUser.UserName = item.ApplicationUser.UserName;
		}
		
		public void Dispose()
		{
			_context.Dispose();
		}
	}
}
