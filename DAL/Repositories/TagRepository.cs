﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
	public class TagRepository : ITagRepository
	{
		private readonly BlogContext _context;
		public TagRepository(BlogContext context)
		{
			_context = context;
		}

		public async Task AddToArticleAsync(string name,int articleId)
		{
			var result = await _context.Articles.FirstAsync(s=>s.Id == articleId);
			var tag = await _context.Tags.FirstOrDefaultAsync(s => s.Name.ToLower() == name.ToLower());
			if (result != null)
			{
				if (tag is null)
				{
					var item = new Tag { Name = name };
					_context.Tags.Add(item);
					result.Tags.Add(item);
				}
				else
				{
					result.Tags.Add(tag);
				}
				
			}
		}

		public void Create(Tag item)
		{
			_context.Tags.Add(item);
		}

		public void Delete(int id)
		{
			_context.Tags.Remove(_context.Tags.Find(id));
		}

		public async Task<Tag> GetItemByIdAsync(string id)
		{
			var id1 = int.Parse(id);
			return await _context.Tags.FindAsync(id1);
		}
		public async Task<Tag> GetItemByIdWithDetailsAsync(string id)
		{
			var id1 = int.Parse(id);
			return await _context.Tags.Include(s=>s.Articles).FirstAsync(s=>s.Id==id1);
		}
		public async Task<IEnumerable<Tag>> GetItemsAsync()
		{
			return await _context.Tags.ToListAsync();
		}
		public async Task<IEnumerable<Tag>> GetItemsWithDetailsAsync()
		{
			return await _context.Tags.Include(s=>s.Articles).ToListAsync();
		}
		public async Task RemoveFromArticleAsync(string name, int articleId)
		{
			var result = await _context.Articles.Include(s=>s.Tags).FirstOrDefaultAsync(s=>s.Id==articleId);
			if (result != null)
			{
				var tag = result.Tags.First(s => s.Name == name);
				result.Tags.Remove(tag);
				await _context.SaveChangesAsync();
			}
		}

		public async void Update(Tag item)
		{
			var result = await _context.Tags.FindAsync(item.Id);
			if (result != null)
			{
				result.Name = item.Name;
			}
		}
		public void Dispose()
		{
			_context.Dispose();
		}
	}
}
