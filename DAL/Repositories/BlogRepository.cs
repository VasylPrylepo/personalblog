﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DAL.Repositories
{
	public class BlogRepository : IRepository<Blog>
	{
		private readonly BlogContext _context;
		public BlogRepository(BlogContext context)
		{
			_context = context;
		}
		public void Create(Blog item)
		{
			var result = _context.Clients.Find(item.ProfileId);
			item.ClientProfile = result;
			_context.Blogs.Add(item);	
		}

		public void Delete(int id)
		{
			_context.Blogs.Remove(_context.Blogs.Find(id));
		}

		public void Dispose()
		{
			_context.Dispose();
		}

		public async Task<Blog> GetItemByIdAsync(string id)
		{
			var id1 = int.Parse(id);
			return await _context.Blogs.FindAsync(id1);
		}
		public async Task<Blog> GetItemByIdWithDetailsAsync(string id)
		{
			var id1 = int.Parse(id);
			return await _context.Blogs.Include(s => s.Articles).Include(s => s.ClientProfile).FirstAsync(s => s.Id == id1);
		}

		public async Task<IEnumerable<Blog>> GetItemsAsync()
		{
			return await _context.Blogs.ToListAsync();
		}
		public async Task<IEnumerable<Blog>> GetItemsWithDetailsAsync()
		{
			return await _context.Blogs.Include(s => s.Articles).Include(s => s.ClientProfile).ToListAsync();
		}
		public void Update(Blog item)
		{
			var blog = _context.Blogs.Find(item.Id);
			blog.Name = item.Name;
			blog.ProfileId = item.ProfileId;
		}
	}
}
