﻿using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
	public class ArticleRepository : IRepository<Article>
	{
		private readonly BlogContext _context;
		public ArticleRepository(BlogContext context)
		{
			_context = context;
		}
		public void Create(Article item)
		{
			item.ClientProfile = _context.Clients.Find(item.ProfileId);
			item.Blog = _context.Blogs.Find(item.BlogId);
			var currentTags =  _context.Tags;
			var newTags = new List<Tag>();
			foreach(var tag in item.Tags)
			{
				if (!currentTags.Any(s => s.Name.ToLower() == tag.Name.ToLower()))
				{
					_context.Tags.Add(tag);
					newTags.Add(tag);
				}
				else
				{
					newTags.Add(currentTags.First(s => s.Name.ToLower() == tag.Name.ToLower()));
				}
			}
			item.Tags = newTags;
			_context.Articles.Add(item);
		}

		public void Delete(int id)
		{
			_context.Articles.Remove(_context.Articles.Find(id));
		}

		public void Dispose()
		{
			_context.Dispose();
		}

		public async Task<Article> GetItemByIdAsync(string id)
		{
			var id1 = int.Parse(id);
			return await _context.Articles.FirstAsync(s => s.Id == id1);
		}
		public async Task<Article> GetItemByIdWithDetailsAsync(string id)
		{
			var id1 = int.Parse(id);
			return await _context.Articles
				.Include(s => s.Comments.Select(f=>f.ClientProfile))
				.Include(s => s.Tags)
				.Include(s => s.Blog)
				.Include(s=>s.ClientProfile)
				.FirstAsync(s => s.Id == id1);
		}
		public async Task<IEnumerable<Article>> GetItemsAsync()
		{
			return await _context.Articles.Include(s=>s.Blog).Include(s=>s.ClientProfile).Include(s=>s.Tags).ToListAsync();
		}
		public async Task<IEnumerable<Article>> GetItemsWithDetailsAsync()
		{
			return await _context.Articles
				.Include(s => s.Comments)
				.Include(s => s.Tags)
				.Include(s => s.Blog)
				.Include(s => s.ClientProfile)
				.ToListAsync();
		}

		public void Update(Article item)
		{
			var article = _context.Articles.Find(item.Id);
			article.ProfileId = item.ProfileId;
			article.Text = item.Text;
			article.Title = item.Title;
			article.DateTime = item.DateTime;
		}
	}
}
