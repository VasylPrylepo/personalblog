﻿using DAL.EF;
using DAL.Entities;
using DAL.Identity;
using DAL.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Threading.Tasks;

namespace DAL.Repositories
{
	public class UnitOfWork : IUnitOfWork
	{
		private readonly BlogContext _context;
		public IRepository<Article> Articles { get; private set; }

		public IRepository<Comment> Comments { get; private set; }

		public IRepository<ClientProfile> Profiles { get; private set; }
		public IRepository<Blog> Blogs { get; private set; }

		public ITagRepository Tags { get; private set; }

		public ApplicationUserManager UserManager { get; private set; }

		public ApplicationRoleManager RoleManager { get; private set; }
		public UnitOfWork(BlogContext context, IUserStore<ApplicationUser> store, IDataProtectionProvider dataProtectionProvider,RoleStore<ApplicationRole> roles)
		{
			_context = context;
			
			UserManager = new ApplicationUserManager(store,dataProtectionProvider);
			RoleManager = new ApplicationRoleManager(roles);
			Articles = new ArticleRepository(_context);
			Comments = new CommentRepository(_context);
			Profiles = new ProfileRepository(_context);
			Tags = new TagRepository(_context);
			Blogs = new BlogRepository(_context);
		}
		public async Task SaveAsync()
		{
			await _context.SaveChangesAsync();
		}
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		public virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					Articles.Dispose();
					Tags.Dispose();
					Comments.Dispose();
					Profiles.Dispose();
				}
				this.disposed = true;
			}
		}
		private bool disposed = false;

		
	}
}
