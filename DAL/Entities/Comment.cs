﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Entities
{
	public class Comment
	{
		public int Id { get; set; }
		public string Text { get; set; }
		public DateTime DateTime { get; set; }
		[ForeignKey("ClientProfile")]
		public string ProfileId { get; set; }
		public virtual ClientProfile ClientProfile { get; set; }
		public int ArticleId { get; set; }
		public Article Article { get; set; }
	}
}
