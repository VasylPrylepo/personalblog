﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Entities
{
	public class Article
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Text { get; set; }
		public DateTime DateTime { get; set; }
		[ForeignKey("ClientProfile")]
		public string ProfileId { get; set; }
		public ClientProfile ClientProfile { get; set; }
		[ForeignKey("Blog")]
		public int BlogId { get; set; }
		public Blog Blog { get; set; }
		
		public ICollection<Comment> Comments { get; set; }
		public ICollection<Tag> Tags { get; set; }
	}
}
