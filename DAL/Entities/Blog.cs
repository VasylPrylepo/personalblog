﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Entities
{
	public class Blog
	{		
		public int Id { get; set; }

		[ForeignKey("ClientProfile")]
		public string ProfileId { get; set; }
		public virtual ClientProfile ClientProfile { get; set; }
		public string Name { get; set; }
		public DateTime DateTime { get; set; }
		public ICollection<Article> Articles { get; set; }
	}
}
