﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;

namespace PL.Filters
{
    public class AllowAnonymousOnlyAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new RedirectResult("~/Error/Forbiddent");
            }
        }
		protected override bool AuthorizeCore(HttpContextBase httpContext)
		{
			return false;
		}
	}
}