﻿using BLL.Interfaces;
using DAL.Interfaces;
using DAL.Repositories;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace PL.Filters
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new RedirectResult("~/Account/Login");
            }
            else if (Roles !=  "" && filterContext.HttpContext.User.IsInRole("user"))
            {
                filterContext.Result = new RedirectResult("~/Error/Forbiddent");
            }                      
        }
		protected override bool AuthorizeCore(HttpContextBase httpContext)
		{
			return false;
		}
	}
}