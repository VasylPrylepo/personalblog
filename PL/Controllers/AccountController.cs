﻿using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using PL.Filters;
using PL.Models;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PL.Controllers
{
    /// <summary>
    /// Class that provide all functionality related to User.
    /// </summary>
    public class AccountController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUserService _service;
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        public AccountController(IUserService service,IMapper mapper)
		{
            _service = service;
            _mapper = mapper;
		}
        /// <summary>
        /// Give us List of all users that in our system.
        /// </summary>
        /// <param name="name">Optional to get User with exact UserName.</param>
        /// <returns>View with all Users.</returns>
        [CustomAuthorize(Roles="admin")]
        public async Task<ActionResult> Index(string name)
		{
            var roles = _service.GetRoles();
            var result = await _service.GetClients();
            if (name != null)
            {
                result = result.Where(s => s.UserName.ToLower() == name.ToLower());
            }
            foreach (var item in result)
			{
                item.RoleName = roles[item.RoleId];
			}
			
            return View(result.OrderBy(s=>s.UserName));
		}
        /// <summary>
        /// Give us information about exact User.
        /// </summary>
        /// <param name="id">Id of User.</param>
        /// <returns>View with information.</returns>
        public async Task<ActionResult> Info(string id)
		{
            ProfileDto profile;

            var userId = id is null ? User.Identity.GetUserId() : id;

            var roles = _service.GetRoles();

            profile = await _service.GetProfileByIdAsync(userId);

            profile.RoleName = roles[profile.RoleId];

            ViewBag.IsConfirmed = await _service.IsConfirmed(userId);

            return View(profile);
		}
        /// <summary>
        /// Give use editing form, that filled with info.
        /// </summary>
        /// <param name="id">Id of User.</param>
        /// <returns>View with editing form.</returns>
        [CustomAuthorize]
        public async Task<ActionResult> Edit(string id)
		{
            ProfileDto profile1;
            if (id is null)
            {
                profile1 = await _service.GetProfileByIdAsync(User.Identity.GetUserId());
                
            }
            else
            {
                profile1 = await _service.GetProfileByIdAsync(id);
            }
           
            return View(_mapper.Map<RegisterModel>(profile1));
        }
        /// <summary>
        /// Post method that pass our parameters to BLL.
        /// </summary>
        /// <param name="model">Filled editing form.</param>
        /// <returns>Redirect to Info Action, or return editing form with errors.</returns>
		[HttpPost]
        [CustomAuthorize]
		public async Task<ActionResult> Edit(RegisterModel model)
		{
            ModelState["Password"].Errors.Clear();
            ModelState["ConfirmPassword"].Errors.Clear();
            try
			{
                model.Id = User.Identity.GetUserId();
                var callbackUrl = Url.Action("ConfirmEmail","Account",null, Request.Url.Scheme);

                var claim = await _service.UpdateProfile(_mapper.Map<ProfileDto>(model),callbackUrl);
                AuthenticationManager.SignOut();
                Session.Clear();
                Session.RemoveAll();
                Session.Abandon();
                AuthenticationManager.SignIn(new AuthenticationProperties
                {
                    IsPersistent = true
                }, claim);
            }
			catch(Exception ex)
			{
                ModelState.AddModelError("",ex.Message);
			}
			if (ModelState.IsValid)
			{
                return RedirectToAction("Index", "Article");
            }
            return View(model);
		}
        /// <summary>
        /// Show login form.
        /// </summary>
        /// <returns>View with login form.</returns>
        [AllowAnonymousOnly]
		public ActionResult Login()
        {
            return View();
        }
        /// <summary>
        /// Post method with data which passed to BLL. 
        /// </summary>
        /// <param name="model">Form with data.</param>
        /// <returns>Redirect to Articles, or return login form with errors.</returns>
        [HttpPost]
        [AllowAnonymousOnly]
        public async Task<ActionResult> Login(LoginModel model)        
        {
            if (ModelState.IsValid)
            {
                ProfileDto userDto = new ProfileDto { UserName = model.Name, Password = model.Password };
                ClaimsIdentity claim = await _service.Authenticate(userDto);
                if (claim == null)
                {
                    ModelState.AddModelError("", "Invalid login or password");
                }
                else
                {
                    AuthenticationManager.SignOut();
                    Session.Clear();
                    Session.RemoveAll();
                    Session.Abandon();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, claim);
                    return RedirectToAction("Index", "Article");

                }
            }
            return View(model);
        }
        /// <summary>
        /// Logout and clear all info about current user.
        /// </summary>
        /// <returns>Redirect to Articles.</returns>
        [CustomAuthorize]
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            return RedirectToAction("Index", "Article");
        }
        /// <summary>
        ///Show us Register form.
        /// </summary>
        /// <returns>View with Register form.</returns>
        [AllowAnonymousOnly]
        public ActionResult Register()
        {
            return View();
        }
        /// <summary>
        /// Post method that pass info to BLL.
        /// </summary>
        /// <param name="model">Filled Register form.</param>
        /// <returns>Form with specific text.</returns>
        [HttpPost]
        [AllowAnonymousOnly]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var callbackUrl = Url.Action("ConfirmEmail","Account", null, Request.Url.Scheme);

                    var succes = await _service.Create(_mapper.Map<ProfileDto>(model), callbackUrl);
                    ViewBag.Text = succes ? "See link to confirm email in your email." : "Something wrong with your email, check it.";                    
                    return View();                    
                }
                catch(Exception ex)
                {
                    ModelState.AddModelError("",ex.Message); 
                }
                   
            }
            return View(model);
        }
        /// <summary>
        /// Method to confirm email.
        /// </summary>
        /// <param name="userId">User's id.</param>
        /// <param name="code">EmailConfirmation Token.</param>
        /// <returns>Redirect to Articles,or page with error.</returns>
        [HttpGet]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToAction("BadRequest", "Error");
            }
            var user = await _service.GetProfileByIdAsync(userId.Base64ForUrlDecode());
            if (user == null)
            {
                return RedirectToAction("BadRequest", "Error");
            }
            var result = await _service.Confirm(userId, code);
            if (result.Succeeded)
                return RedirectToAction("Index", "Article");
            else
                return RedirectToAction("BadRequest", "Error");
        }
        /// <summary>
        /// Post method that change profile to "opposite" role.
        /// Change our identity info, if user is current.
        /// </summary>
        /// <param name="profileId">User's id</param>
		[HttpPost]
        [CustomAuthorize(Roles = "admin")]
        public async Task ChangeRole(string profileId)
		{
            var claim =  await _service.ChangeRole(profileId);
            
            if (profileId == User.Identity.GetUserId())
            {
                AuthenticationManager.SignOut();
                Session.Clear();
                Session.RemoveAll();
                Session.Abandon();
                AuthenticationManager.SignIn(new AuthenticationProperties
                {
                    IsPersistent = true
                }, claim);
            }
        }
	}
}