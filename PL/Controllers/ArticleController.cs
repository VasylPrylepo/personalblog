﻿using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using Microsoft.AspNet.Identity;
using PagedList;
using PL.Filters;
using PL.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PL.Controllers
{
	/// <summary>
	/// Class that provide all functionality related to Article.
	/// </summary>
	public class ArticleController : Controller
	{
		private readonly IArticleService _service;
		private readonly IMapper _mapper;
		public ArticleController(IArticleService service,IMapper mapper)
		{
			_service = service;
			_mapper = mapper;
		}
		/// <summary>
		/// Give us List of all Articles that in our system.
		/// </summary>
		/// <param name="sort">Tell us how to sort.</param>
		/// <param name="blogId">Tell us to which blog articles related.</param>
		/// <param name="tag">Tell us which tags Articles should have.</param>
		/// <param name="page">For pagination.</param>
		/// <returns>View with articles.</returns>
		public async Task<ActionResult> Index(Sorting? sort, int? blogId,string tag,int?page)		
		{
			ViewBag.Tags = await _service.GetTagsAsync();
			IEnumerable<ArticleDto> list;

			if (sort is null && tag is null && blogId is null)
			{
				list = await _service.GetArticles(Sorting.Date, null, null);
			}
			else
			{
				list = await _service.GetArticles(sort, tag, blogId); 
			}

			ViewBag.Name = tag;
			ViewBag.blog = blogId;
			ViewBag.Sort = sort;
			ViewBag.Page = page ?? 1;
			return View(list.ToPagedList(page??1, 4));			
		}
		/// <summary>
		/// Return articles with specific name with all our filters.
		/// </summary>
		/// <param name="title">Title of Articles.</param>
		/// <param name="page">For pagination</param>
		/// <param name="blogId">Id of current blog.</param>
		/// <param name="tag">Articles related to this tag.</param>
		/// <returns>Articles with same name.</returns>
		[HttpPost]
		public async Task<PartialViewResult> Index(string title,int? page, int? blogId, string tag)
		{
			var list = await _service.GetArticles(Sorting.Date, tag, blogId);
			list = list.Where(s => s.Title.ToLower() == title.ToLower());
			ViewBag.Tags = await _service.GetTagsAsync();
			return PartialView("_ArticlePartial", list.ToPagedList(page ?? 1, 4));
		}
		/// <summary>
		/// Get Article by id.
		/// </summary>
		/// <param name="id">Article's id.</param>
		/// <returns>View with Article.</returns>
		[ActionName("Find")]
		public async Task<ActionResult> GetById(int id)
		{
			var article = await _service.GetArticleByIdAsync(id);
			ViewBag.Tags = await _service.GetTagsAsync();
			ViewBag.Article = article;
			return View(new CommentModel());
		}
		/// <summary>
		/// Method provide info to add new Article.
		/// </summary>
		/// <returns>Article form.</returns>
		[CustomAuthorize]
		public async Task<ActionResult> AddArticle()
		{			
			var tags = await _service.GetTagsAsync();
			ViewBag.Blogs =  new SelectList(await _service.GetBlogsAsync(), "id", "Name");
			var item = new ArticleModel();
			item.TagsName = new Dictionary<string, bool>();
			tags.ToList().ForEach(s => item.TagsName.Add(s.Name, false));
			return View(item);
		}
		/// <summary>
		/// Post method that analyze info that comes from Article form.
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		[HttpPost]
		[CustomAuthorize]
		public async Task<ActionResult> AddArticle(ArticleModel model)
		{
			List<TagDto> tags = new List<TagDto>();
			foreach(var tag in model.TagsName)
			{
				if (tag.Value)
				{
					tags.Add(new TagDto { Name = tag.Key });
				}
			}
			model.Tags = tags;
			model.TagsName = null;
			model.ProfileId = User.Identity.GetUserId();
			await _service.AddArticleAsync(_mapper.Map<ArticleDto>(model));
			return RedirectToAction("Index");
		}
		/// <summary>
		/// Add comment to article.
		/// </summary>
		/// <param name="model">Comment form.</param>
		/// <returns>Partial view with new list of comments.</returns>
		[HttpPost]
		[ActionName("Find")]
		[CustomAuthorize]
		public async Task<PartialViewResult> AddComment(CommentModel model)		
		{
			
			if (ModelState.IsValid)
			{
				
				var id = HttpContext.GetOwinContext().Authentication.User.Identity.GetUserId();

				model.ProfileId = id;

				await _service.AddCommentToArticleAsync(_mapper.Map<CommentDto>(model));

				var article1 = await _service.GetArticleByIdAsync(model.ArticleId);
				ModelState.Clear();
				return PartialView("_CommentsPartial", article1.Comments);
			}
			var article = await _service.GetArticleByIdAsync(model.ArticleId);
			return PartialView("_CommentsPartial", article.Comments);
		}
		/// <summary>
		/// Add tag to Article.
		/// </summary>
		/// <param name="articleId">Article id.</param>
		/// <param name="tag">Tag name.</param>
		/// <returns>Partial view with tags.</returns>
		[HttpPost]
		[CustomAuthorize]
		public async Task<PartialViewResult> AddTagToArticle(int articleId, string tag)
		{
			var taglist = await _service.GetTagsAsync();

			var article = await _service.GetArticleByIdAsync(articleId);
			TagDto tag1 = null;

			if (tag == "")
			{
				ModelState.AddModelError("", "You can't input empty");
			}
			if (taglist.Any(s => s.Name.ToLower() == tag.ToLower()))
			{
				if (article.Tags.Any(s => s.Name.ToLower() == tag.ToLower()))
				{
					ModelState.AddModelError("", "You have similar tag");
				}
				else
				{
					tag1 = new TagDto { Name = tag, ArticleId = articleId};
				}
			}
			else
			{
				tag1 = new TagDto { Name = tag, ArticleId = articleId };
				_ = taglist.Append(tag1);
			}
			if(article is null)
			{
				ModelState.AddModelError("", "Invalid id");
			}
			
			if (ModelState.IsValid)
			{
				await _service.AddTagToArticleAsync(tag1);
				article?.Tags.Add(tag1);				
			}

			ViewBag.Tags = taglist;

			return PartialView("_IndexPartial", article);
		}
		/// <summary>
		/// Delete tag from article.
		/// </summary>
		/// <param name="articleId">Article id.</param>
		/// <param name="tag">Tag name.</param>
		/// <returns>Partial view with list of tags.</returns>
		[HttpPost]
		[CustomAuthorize]
		public async Task<PartialViewResult> DeleteTagFromArticle(int articleId, string tag)
		{
			var article = await _service.GetArticleByIdAsync(articleId);

			var tag1 = new TagDto{ Name = tag, ArticleId = articleId };			

			article.Tags.Remove(article.Tags.Single(s => s.Name == tag));

			var taglist = await _service.GetTagsAsync();

			await _service.DeleteTagFromArticleAsync(tag1);

			ViewBag.Tags = taglist;

			return PartialView("_IndexPartial", article);
		}
		/// <summary>
		/// Delete specific comment.
		/// </summary>
		/// <param name="commentId">Comment id.</param>
		[HttpPost]
		public async Task DeleteCommment(int commentId)
		{
			await _service.DeleteCommentFromArticleAsync(commentId);
		}
	}
}