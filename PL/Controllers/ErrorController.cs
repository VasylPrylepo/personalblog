﻿
using System.Web.Mvc;

namespace PL.Controllers
{
    /// <summary>
    /// Controller that give us some costume error pages.
    /// </summary>
    public class ErrorController : Controller
    {
        public ActionResult ServerError()
        {
            return View();
        }

        public ActionResult NotFound()
        {
            return View();
        }
        public ActionResult Forbiddent()
        {
            return View();
        }

        public ActionResult BadRequest()
        {
            return View();
        }
    }
}