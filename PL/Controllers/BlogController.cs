﻿using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using Microsoft.AspNet.Identity;
using PL.Filters;
using PL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PL.Controllers
{
	/// <summary>
	/// Functionality related to Blog.
	/// </summary>
    public class BlogController : Controller
    {
        private readonly IBlogService _service;
		private readonly IMapper _mapper;
		public BlogController(IBlogService service,IMapper mapper )
		{
            _service = service;
			_mapper = mapper; 
		}
		/// <summary>
		/// Show all blogs.
		/// </summary>
		/// <param name="sort">Sort parameter.</param>
		/// <param name="name">Name of blog.</param>
		/// <returns>View with blogs.</returns>
		public async Task<ActionResult> Index(Sorting? sort,string name)
		{
			IEnumerable<BlogDto> list = await _service.GetBlogsAsync(sort);
			if (name != null)
			{
				list = list.Where(s => s.Name.ToLower() == name.ToLower());
				ViewBag.Name = name;
			}
			return View(list);
		}
		/// <summary>
		/// Add blog form.
		/// </summary>
		/// <returns>View with blog form.</returns>
		[CustomAuthorize]
		public ActionResult AddBlog()
		{
			return View();
		}
		/// <summary>
		/// Post method adding blog.
		/// </summary>
		/// <param name="model">Blog from.</param>
		/// <returns>Redirect to Index method.</returns>
		[HttpPost]
		[CustomAuthorize]
		public async Task<ActionResult> AddBlog(BlogModel model)
		{
			if (ModelState.IsValid)
			{
				model.ProfileId = User.Identity.GetUserId();
				await _service.CreateBlog(_mapper.Map<BlogDto>(model));
				return RedirectToAction("Index");
			}
			else
			{
				return View();
			}
			
		}
	}
}