﻿using AutoMapper;
using BLL.DTO;
using DAL.Entities;
using PL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PL.Automapper
{
	public class AutomapperProfile:Profile
	{
		public AutomapperProfile()
		{
			CreateMap<Comment, CommentDto>()
				.ForMember(s=>s.UserName,f=>f.MapFrom(t=>t.ClientProfile.ApplicationUser.UserName))
				.ReverseMap();

			CreateMap<CommentModel, CommentDto>().ReverseMap();

			CreateMap<Article, ArticleDto>()
				.ForMember(f=>f.BlogName,s=>s.MapFrom(t=>t.Blog.Name))
				.ForMember(f=>f.BlogId,s=>s.MapFrom(t=>t.BlogId))
				.ReverseMap();

			CreateMap<Tag, TagDto>().ReverseMap();

			CreateMap<ClientProfile, ProfileDto>()
				.ForMember(s=>s.Email,f=>f.MapFrom(t=>t.ApplicationUser.Email))
				.ForMember(f=>f.UserName,s=>s.MapFrom(t=>t.ApplicationUser.UserName))
				.ForMember(f=>f.RoleId,s=>s.MapFrom(t=>t.ApplicationUser.Roles.FirstOrDefault().RoleId))
				.ReverseMap();

			CreateMap<ArticleModel, ArticleDto>()
				.ReverseMap();

			CreateMap<Blog, BlogDto>().ReverseMap();

			CreateMap<BlogModel, BlogDto>();

			CreateMap<RegisterModel, ProfileDto>().ReverseMap();
		}
	}
}
