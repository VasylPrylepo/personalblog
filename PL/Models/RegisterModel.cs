﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace PL.Models
{
	public class RegisterModel
	{
        public string Id { get; set; }
        [Required(ErrorMessage = "This field is required.")]
        [EmailAddress(ErrorMessage ="Input correct email address.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        [RegularExpression(@"^(?:[A-Z a-z0-9@_]{2,20})", ErrorMessage="Only: a-z, 0-9, @, _, characters are allowed. Range: 2-20 symbols")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        [MinLength(6,ErrorMessage ="Password should be at least 6 symbols in length.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        [DataType(DataType.Password)]
        [Compare("Password",ErrorMessage ="Passwords is not equals.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        [RegularExpression(@"^(?:[a-zA-Z]){3,30}\s+\b(street|avenue|square)\b", ErrorMessage ="Expected format is: Name street.")]
        public string Address { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        [RegularExpression(@"^(?:[A-Z a-z]{2,15} ?\b){2,3}$", ErrorMessage = "Expected format: John Brandy or John Brandy Paul.")]
        public string Name { get; set; }
    }
}