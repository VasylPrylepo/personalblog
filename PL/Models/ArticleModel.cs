﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PL.Models
{
	public class ArticleModel
	{
		[Required(ErrorMessage = "Input title")]
		public string Title { get; set; }
		[Required(ErrorMessage = "Input text")]
		[MinLength(300, ErrorMessage = "Min Length")]
		public string Text { get; set; }
		public int BlogId { get; set; }
		public DateTime DateTime { get; set; } = DateTime.Now;
		public string ProfileId { get; set; }
		public Dictionary<string,bool> TagsName { get; set; }
		public ICollection<TagDto> Tags { get; set; } = new List<TagDto>();
	}
}