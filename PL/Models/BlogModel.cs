﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PL.Models
{
	public class BlogModel
	{
		[Required(ErrorMessage = "This field is required")]
		[MinLength(3,ErrorMessage ="Min length is 3")]
		public string Name { get; set; }
		public DateTime DateTime { get; set; } = DateTime.Now;
		public string ProfileId { get; set; }
	}
}