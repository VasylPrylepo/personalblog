﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PL.Models
{
	public class CommentModel
	{
		[Required(ErrorMessage ="Text is required")]
		[StringLength(100,MinimumLength =5 ,ErrorMessage ="Min length is 5 and max is 100")]
		public string Text { get; set; }
		public int ArticleId { get; set; }
		public string ProfileId { get; set; }
		public DateTime DateTime { get; set; } = DateTime.Now;

	}
}