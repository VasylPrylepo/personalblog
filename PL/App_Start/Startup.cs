﻿using BLL.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataProtection;
using Ninject;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

[assembly: OwinStartup(typeof(PL.App_Start.Startup))]
namespace PL.App_Start
{
	public class Startup
	{
        public void Configuration(IAppBuilder app)
        {
            var service = DependencyResolver.Current.GetService<IUserService>();
            app.CreatePerOwinContext(()=> service);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie
            });
        }
    }
}