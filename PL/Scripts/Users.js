﻿function FindUser() {
	let name = document.getElementById("find").value;
	document.getElementById("find").value = "";
	if (name != "") {
		//let url = '@Url.Action("Index","Account",null, Request.Url.Scheme)?name=' + name;
		window.location.href = window.location.protocol +'//'+ window.location.host + "/Account/Index/?name=" + name;
	}
}

function some() {
	console.log("some");
	if (event.target.matches("td.btn")) {
		let id = event.target.closest("tr").children[2].children[0].name;
		let element = event.target.closest("tr").children[3];
		let role = element.innerText == "admin" ? "user" : "admin";
		$.ajax({
			url: '/Account/ChangeRole',
			type: "POST",
			dataType: 'text',
			data: { profileId: id },
			success: function () {
				element.innerHTML = role;
			},
			error: function (xhr) {
				console.log("error");
				console.log(xhr);
			}
		});
	}
}