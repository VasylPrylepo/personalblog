﻿function addTagToArticle() {
	if (event.target.matches("div.dropdown-menu a") || event.target.matches("div.dropdown-item .btn-warning")) {

		var text;
		if (event.target.matches("div.dropdown-item .btn-warning")) {
			text = event.target.closest("div.dropdown-item").children[0].value;
			
		}

		else {
			text = event.target.textContent;
			text = text.substring(1);
			
		}
		
		console.log(event.target.closest("div.dropdown-menu"));
		let id = event.target.closest("div.dropdown-menu").children[0].name;
		var element = event.target.closest(".partialview")
		console.log(text);
		console.log(id);
		console.log(element);

		$.ajax({
			url: '/Article/AddTagToArticle',
			type: "POST",
			dataType: 'text',
			data: { articleId: id, tag: text },
			success: function (data) {
				console.log("success");
				element.innerHTML = data;
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log("error");
				console.log(xhr);
			}
		});
	}

}
function deleteTagFromArticle() {
	if (event.target.classList.contains("close1")) {

		let text = event.target.closest(".btn").textContent.replace(/\s+/g, '');
		text = text.substring(1, text.length - 1);

		let id = event.target.closest("div.d-inline-block").children[0].name;
		console.log(id);

		var element = event.target.closest(".partialview");

		console.log(text);
		console.log(id);

		event.target.sto
		$.ajax({
			url: '/Article/DeleteTagFromArticle',
			type: "POST",
			dataType: 'text',
			data: { articleId: id, tag: text },
			success: function (data) {
				console.log("success");
				element.innerHTML = data;
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log("error");
				console.log(xhr);
			}
		});
	}
	else if (event.target.matches("div.d-inline-block button") || event.target.matches("span.text-dark")) {
		let uri = window.location.href;
		var separator = uri.indexOf('?') !== -1 ? "&" : "?";
		let tag = event.target.closest("div.d-inline-block").textContent.replace(/\s+/g, '');
		tag = tag.substring(1, tag.length - 1);
		if (uri.includes("Find")) {
			location.href = "/Article/Index/?tag=" + tag;
		}
		else {
			let item = uri.substring(uri.indexOf('tag'), uri.indexOf(' ', uri.indexOf('tag')));
			if (item == "") {
				location.href = uri + separator + "tag=" + tag;
			}
			else {
				location.href = item + "tag=" + tag;
			}
			
        }
    }
	return false;
}

function Find() {
	let name = document.getElementById("find").value;
	console.log(name);
	document.getElementById("find").value = "";
	let element = document.getElementById("partial");
	let queryString = window.location.search;
	let urlParams = new URLSearchParams(queryString);
	let tag = urlParams.get('tag');
	let blog = urlParams.get('blogId');
	if (name != "") {
		$.ajax({
			url: '/Article/Index',
			type: "POST",
			dataType: 'text',
			data: { title: name, tag: tag, blogId: blog },
			success: function (data) {
				console.log("success");
				element.innerHTML = data;
				document.getElementsByClassName('list-group')[0].addEventListener("click", function () { addTagToArticle() }, true);
				document.getElementsByClassName('list-group')[0].addEventListener("click", function () { deleteTagFromArticle() }, true);
			},
			error: function (xhr) {
				console.log("error");
				console.log(xhr);
			}
		});
	}
}
function DeleteComment() {
	if (event.target.matches("span.float-right")) {
		let elem = event.target.closest("div.border");
		let id = Number(elem.children[0].name);
		console.log(elem);
		console.log(id);
		$.ajax({
			url: '/Article/DeleteCommment',
			type: "POST",
			dataType: 'text',
			data: { commentId: id },
			success: function () {
				elem.remove();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log("error");
				console.log(xhr);
			}
		});
	}
}