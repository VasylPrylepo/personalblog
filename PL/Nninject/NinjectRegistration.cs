﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Services;
using DAL.EF;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.DataProtection;
using Ninject;
using Ninject.Modules;
using PL.Automapper;

namespace PL.Nninject
{
	public class NinjectRegistration:NinjectModule
	{
		public override void Load()
		{
			var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile<AutomapperProfile>(); });
			Bind<IMapper>().ToConstructor(s => new Mapper(mapperConfiguration));

			Bind<BlogContext>().ToSelf().InTransientScope();

			Bind<IUserStore<ApplicationUser>>().ToMethod(_ => new UserStore<ApplicationUser>(this.Kernel.Get<BlogContext>())).InSingletonScope();
			Bind<IDataProtectionProvider>().To<DpapiDataProtectionProvider>().InSingletonScope();
			Bind<RoleStore<ApplicationRole>>().ToMethod(_ => new RoleStore<ApplicationRole>(this.Kernel.Get<BlogContext>())).InSingletonScope();

			Bind<IUnitOfWork>().To<UnitOfWork>().InTransientScope();

			Bind<IUserService>().To<UserService>().InTransientScope();
			Bind<IArticleService>().To<ArticleService>().InSingletonScope();
			Bind<IBlogService>().To<BlogService>().InSingletonScope();
			

		}
	}
}