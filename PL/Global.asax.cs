﻿using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;
using PL.Nninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PL
{
	public class MvcApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			NinjectModule registrations = new NinjectRegistration();
			var kernel = new StandardKernel(registrations);
			kernel.Unbind<ModelValidatorProvider>();
			DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
		}
	}
}
